# Bitbucket Server Protect Unmerged Branch Hook

A pre-receive hook that stops any push that deletes a branch involved in an active
(not merged/declined) pull request.

## Minimum Requirements

* Java 8
* Apache Maven 3
* Atlassian Plugin SDK 6
